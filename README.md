Very simple python script to forward [Gotify](https://github.com/gotify/server) messages to the linux desktop using LibNotify.

## License
Unlicense, see LICENSE file

## Usage
0. Install required packages from pip (see requirements.txt)
1. Create a client in your gotify instance.
2. Create a file called "url.txt"
3. Insert the URI + token to this file, like this: `wss://gotify.your.server/stream?token=YOUR_GOTIFY_TOKEN`
4. Run as service or something.
5. ???
6. Profit
