#!/usr/bin/env python

import asyncio
import websockets
import json
import sys
import gi
gi.require_version("Notify", "0.7")
from gi.repository import Notify

try:
	with open("url.txt", "r") as f:
		ws_url = f.readline().strip()
except:
	print("Setup instructions:")
	print("1. Create a client in your gotify instance.")
	print("2. Create a file called \"url.txt\"")
	print("3. Insert the URI + token to this file, example:")
	print(">>> wss://gotify.your.server/stream?token=YOUR_GOTIFY_TOKEN")
	print("4. Profit")
	sys.exit(1)

def parse_message(text):
	try:
		msg = json.loads(text)
	except:
		print("Could not parse: ", text)
		return
	notification = Notify.Notification.new(msg.get("title", ""), msg.get("message", ""), "dialog-information")
	notification.show()

async def main_loop():
	async with websockets.connect(ws_url) as websocket:
		while True:
			parse_message(await websocket.recv())

if not Notify.init("Gotify"):
	sys.exit(1)

asyncio.get_event_loop().run_until_complete(main_loop())
